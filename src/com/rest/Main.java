package com.rest;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
@Path("GetExampleService")
public class Main {
    @GET
    @Path("/cislo/{cislo}")
    @Produces(MediaType.TEXT_HTML)
    public String userName(@PathParam("cislo") String c) {
    	int cislo = Integer.parseInt(c);
    	//Theres set of 4 independent operations that u can choose from and order them as you want, 
    	//the basic order and use of them is this:
    	cislo = firstCheck(cislo);
    	cislo = secondCheck(cislo);
    	cislo = thirdCheck(cislo);
    	cislo = fourthCheck(cislo);
        return "<html><body>" + "<Cislo>" + cislo + "</Cislo>" + "</body></html>";
    }
    
    
    
    private int firstCheck(int cislo) {
		int num = cislo;
		int b = 0;
		int prev = 0;
		while (num > 0) {
			int a = num%10;
			if (a <= 3 && b != 0) {
				cislo = (int) (cislo + ((prev-a)*(Math.pow(10, b+1)/10)));
				cislo-=(prev-a)*(Math.pow(10, b)/10);
			}
			else {
				prev = a;
			}
			num-=a;
			num/=10;
			b++;
		}
		
		return cislo;
	}
    
    private int secondCheck(int cislo) {
    	int num = cislo;
    	int b = 1;
    	while (num>0) {
    		int a = num%10;
    		if (a == 8 || a == 9) {
    			if (b!=1) {
    				int c = cislo%b;
    				cislo-=cislo%(b*10);
    				cislo*=10;
    				int fin = a*2*b;
    				fin+=c;
    				cislo+=fin;
    			}
    			else {
    				cislo-=a;
    				cislo*=10;
    				cislo+=a*2;
    			}
    			b*=10;
    		}
    		b*=10;
    		num-=a;
    		num/=10;
    	}
    	return cislo;
    }
    
    
    //second version of the second operation (with use of an ArrayList, this ArrayList logic can be used in every of the first 2 operations
    private int secondCheck1(int cislo) {
		ArrayList<Integer> arr = new ArrayList<>();
		while(cislo > 0) {
			int a = cislo%10;
			if (a == 8) {
				arr.add(0,6);
				arr.add(0,1);
			}
			else if (a == 9) {
				arr.add(0,8);
				arr.add(0, 1);
			}
			else {
				arr.add(0,a);
			}
			cislo-=a;
			cislo/=10;
		}
		for (int i = 0; i < arr.size(); i++) {
			cislo+=arr.get(i);
			if (i != arr.size()-1) {
			cislo*=10;
			}
		}
		return cislo;
	}
    
  
    
    
    
    private int thirdCheck(int cislo) {
		int num = cislo;
		int b = 1;
		while (num > 0) {
			int a = num%10;
			if (a == 7) {
				if (b != 1) {
				int c = cislo%b;
				cislo-=cislo%(b*10);
				cislo/=10;
				cislo+=c;
				}
				else {
					cislo-=a;
					cislo/=10;
				}
			}
			else {
			b*=10;
			}
			num-=a;
			num/=10;
		}
		return cislo;
	}
    
    private int fourthCheck(int cislo) {
    	int num = cislo;
    	int counter = 0;
    	while (num > 0) {
    		int a = num%10;
    		if (a%2 == 0) {
    			counter++;
    		}
    		num-=a;
    		num/=10;
    	}
    	return cislo/counter;
    }
}
